<?php
/*
Plugin Name: Inifnity Integration
Plugin URI:  http://holocron.it
Description: Plugin che integra il sistema wordpress con ERP Infinity
Version:     1.0
Author:      Holocron 
Author URI:  http://holocron.it
*/

define( 'PHN_PLUG_PATH', __DIR__);

// Composer
require PHN_PLUG_PATH.'/vendor/autoload.php';






/**
 *
 * Carico le classi
 *
 */
foreach (glob(__DIR__."/class/[!_]*.php") as $filename)
{
    include_once $filename;
    
}


/*==================================
=            INIT CLASS            =
==================================*/

$integration = new integrationClass();

/*=====  End of INIT CLASS  ======*/


/**
 *
 * Carico il core del tema
 *
 */
foreach (glob(__DIR__."/core/[!_]*.php") as $filename)
{
    include_once $filename;
    
}


if( is_admin() ){
  require_once PHN_PLUG_PATH.'/admin/admin.php';
}


/*=====  End of AUTO FILE LOADER  ======*/



/**
 *
 * Registro gli script e styles per lato admin
 *
 */
add_action( 'wp_admin_enqueue_scripts', 'PHN_load_admin_scripts' );
function PHN_load_admin_scripts() {
  
  wp_enqueue_style( 'shop-admin-css',  plugin_dir_url( __FILE__ ) . 'assets/css/admin/admin.css', false, '1.0.0' );
  wp_enqueue_script('shop-admin-js',  plugin_dir_url( __FILE__ ) . 'assets/js/admin/admin.js', array('jquery'));
  
}