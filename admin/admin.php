<?php 
/**
 *
 * Questo file contiene tutte le macro funzionalità lato wp-admin
 *
 */


/**
 * Setting page
 */
require_once __DIR__."/settings-api-class.php";
require_once __DIR__."/settings-page-class.php";


/*==================================
=            INIT CLASS            =
==================================*/

new PhnSettingsPage();

/*=====  End of INIT CLASS  ======*/


