<?php

if ( !class_exists('PhnSettingsPage' ) ):
class PhnSettingsPage {

    private $settings_api;

    function __construct() {
        $this->settings_api = new WeDevs_Settings_API;

        add_action( 'admin_init', array($this, 'admin_init') );
        add_action( 'admin_menu', array($this, 'admin_menu') );
        
    }

    function admin_init() {

        //set the settings
        $this->settings_api->set_sections( $this->get_settings_sections() );
        $this->settings_api->set_fields( $this->get_settings_fields() );

        //initialize settings
        $this->settings_api->admin_init();
    }


    function admin_menu() {
        add_options_page( __( 'Pharmanutra', 'phn' ) , __( 'Pharmanutra', 'phn' ) , 'delete_posts', 'phn-settings', array($this, 'plugin_page') );
    }

    function get_settings_sections() {
        $sections = array(
            array(
                'id'    => integrationClass::GENERAL_OPTION_KEY,
                'title' => __( 'Generali', 'phn' )
            ),
            array(
                'id'    => integrationClass::INTEGRATION_OPTION_KEY,
                'title' => __( 'Integrazioni', 'phn' )
            ),
            
        );
        return $sections;
    }

    /**
     * Returns all the settings fields
     *
     * @return array settings fields
     */
    function get_settings_fields() {
        $settings_fields = array(
            integrationClass::GENERAL_OPTION_KEY => array(
                array(
                    'name'        => 'head2' ,
                    'desc'        => __( '<h3>Notifiche</h3><hr>', 'phn' ),
                    'type'        => 'html'
                ),  
                array(
                    'name'    => 'support_email',
                    'label'   => __( 'Email supporto', 'phn' ),
                    'desc'    => __( 'Indirizzi email per le notifiche in caso di problemi sulla parte integrazioni. Inserire più indirizzi separati da virgola (,)', 'phn' ),
                    'type'    => 'text',
                ),
                // array(
                //     'name'        => 'head1' ,
                //     'desc'        => __( '<h3>Comportamento generale</h3><hr>', 'phn' ),
                //     'type'        => 'html'
                // ),
                // array(
                //     'name'  => 'hide_default',
                //     'label' => __( 'Hide Default Lang.', 'phn' ),
                //     'desc'  => __( 'Check this option if you want to hide the language code if is the default', 'phn' ),
                //     'type'  => 'checkbox'
                // ),
                // array(
                //     'name'  => 'enable_cookie',
                //     'label' => __( 'Cookie Lang', 'phn' ),
                //     'desc'  => __( 'If you are not sure of traslating all link in your site, with cookie you can provide a good expirance for your customer mantaining current language. This option is useful only if you don\'t hide the default lang in permalink', 'phn' ),
                //     'type'  => 'checkbox'
                // ),
                // array(
                //     'name'        => 'head2' ,
                //     'desc'        => __( '<h3>Not Tranlsted Content</h3><hr>', 'phn' ),
                //     'type'        => 'html'
                // ),
               
                // array(
                //     'name'    => 'redirect_target_custom',
                //     'label'   => __( 'Redirect Custom Link', 'phn' ),
                //     'desc'    => __( 'Set a custom link for redirect the visitor in case of not-traslated content. <b>Leave empty</b> fot use "Redirect Target"', 'phn' ),
                //     'type'    => 'text',
                //     'placeholder' => __( 'Use absolute url', 'phn' ),
                // ),

                // array(
                //     'name'    => 'redirect_target_custom',
                //     'label'   => __( 'Redirect Custom Link', 'phn' ),
                //     'desc'    => __( 'Set a custom link for redirect the visitor in case of not-traslated content. <b>Leave empty</b> fot use "Redirect Target"', 'phn' ),
                //     'type'    => 'text',
                //     'placeholder' => __( 'Use absolute url', 'phn' ),
                // ),

                // array(
                //     'name'    => 'default_title',
                //     'label'   => __( 'Default Post Title', 'phn' ),
                //     'desc'    => __( 'Set a custom title to show if content is not translated. Used only if "Show Custom Message" is selected in "If content not trslated?" option', 'phn' ),
                //     'type'    => 'text',
                // ),
                //  array(
                //     'name'    => 'default_content',
                //     'label'   => __( 'Default Post Content', 'phn' ),
                //     'desc'    => __( 'Set a custom content to show if content is not translated. Used only if "Show Custom Message" is selected in "If content not trslated?" option', 'phn' ),
                //     'type'    => 'wysiwyg',
                // ),
            ),
           integrationClass::INTEGRATION_OPTION_KEY => array(
                array(
                    'name'        => 'head2' ,
                    'desc'        => __( '<h3>Infinity</h3><hr>', 'phn' ),
                    'type'        => 'html'
                ),
                array(
                    'name'    => 'infinity_url',
                    'label'   => __( 'Url Infinty', 'phn' ),
                    'desc'    => __( 'Url comprensivo di protocollo e porta. Indica la basepath per le chiamate API verso Infinity', 'phn' ),
                    'type'    => 'text',
                ),
                array(
                    'name'    => 'infinity_appid',
                    'label'   => __( 'App ID', 'phn' ),
                    'desc'    => __( 'App ID per autenticazione verso infinity', 'phn' ),
                    'type'    => 'text',
                ),
                array(
                    'name'    => 'infinity_apikey',
                    'label'   => __( 'Api Key', 'phn' ),
                    'desc'    => __( 'Api Key per autenticazione verso infinity', 'phn' ),
                    'type'    => 'text',
                ),
                array(
                    'name'    => 'infinity_invoice_path',
                    'label'   => __( 'Path Fatture', 'phn' ),
                    'desc'    => __( 'Path relativo alla basepath del sito dove ricercare i file delle fatture popolate da infinity.', 'phn' ),
                    'type'    => 'text',
                ),
                array(
                    'name'        => 'head4' ,
                    'desc'        => __( '<h3>Chiapparoli</h3><hr>', 'phn' ),
                    'type'        => 'html'
                ),
                array(
                    'name'    => 'chiapp_user',
                    'label'   => __( 'User FTP', 'phn' ),
                    'desc'    => __( 'Utente FTP per lo spazio di interscambio', 'phn' ),
                    'type'    => 'text',
                ),
                array(
                    'name'    => 'chiapp_password',
                    'label'   => __( 'Password FTP', 'phn' ),
                    'desc'    => __( 'Password FTP per lo spazio di interscambio', 'phn' ),
                    'type'    => 'text',
                ),
                array(
                    'name'    => 'chiapp_new_order_path',
                    'label'   => __( 'Path Ordini', 'phn' ),
                    'desc'    => __( 'Path relativo alla home dir dell\'ftp per la cartella dove scrivere i tracciati ordine', 'phn' ),
                    'type'    => 'text',
                ),
                array(
                    'name'    => 'chiapp_order_confirm_path',
                    'label'   => __( 'Path Ricevute', 'phn' ),
                    'desc'    => __( 'Path relativo alla home dir dell\'ftp per la cartella dove leggere i tracciati di ritorno', 'phn' ),
                    'type'    => 'text',
                ),

                
                // array(
                //     'name'  => 'enable_slug_translation',
                //     'label' => __( 'Enable Slug Translation?', 'phn' ),
                //     'desc'  => __( 'Check this option if you want to traslate the url of post', 'phn' ),
                //     'type'  => 'checkbox'
                // ),
                // array(
                //     'name'  => 'qtrans_compatibility',
                //     'label' => __( 'Qtranslate-x Compatibility', 'phn' ),
                //     'desc'  => __( 'Check this option if you want to use this plugin with a deep compatibility with Qtraslate-X functions', 'phn' ),
                //     'type'  => 'checkbox'
                // ),

                // array(
                //     'name'  => 'disable_head_meta',
                //     'label' => __( 'Disable Head Meta Tag', 'phn' ),
                //     'desc'  => __( 'Check this option to disable Hololang to generate meta tag in page head (not suggested)', 'phn' ),
                //     'type'  => 'checkbox'
                // ),

                // array(
                //     'name'  => 'enable_log',
                //     'label' => __( 'Enable Log?', 'phn' ),
                //     'desc'  => __( 'Check this option to enable the log of Hololang. Use this option if you have some problem or in development status. Log are stored in /wp-content/hololang/ directory', 'phn' ),
                //     'type'  => 'checkbox'
                // ),
            ),
            


           
        );

        return $settings_fields;
    }

    function plugin_page() {
        echo '<div class="wrap">';

        $this->settings_api->show_navigation();
        $this->settings_api->show_forms();

        echo '</div>';
    }


}
endif;
