<?php


/**
 *
 * METABOX PRODUCT
 *
 */
add_filter( 'rwmb_meta_boxes', 'infinity_product_meta_boxes' );
function infinity_product_meta_boxes( $meta_boxes ) {
	global $integration;

    $meta_boxes[] = array(
    	'id' 			=> 'phn-product-details',
        'title'  		=> 'Informazioni aggiuntive',
        'post_types'	=> 'product',
        'context'    	=> 'normal',
        'priority'   	=> 'high',
        'fields' => array(
            array(
                'id'   => '_phn_color',
                'name' => 'Colore',
                'type' => 'color',
            ),
            array(
                'id'   => '_phn_bundle',
                'name' => 'E\' un bundle? ',
                'type' => 'checkbox',
            ),
        ),
    );

   	$selectErpProductList = array();
    foreach ( $integration->get_erp_products() as $k => $p) {
    	$selectErpProductList[ $p->code ] = $p->desc;
    }

    $meta_boxes[] = array(
    	'id' 			=> 'phn-product-erp',
        'title'  		=> 'ERP - Elenco prodotti listino interno',
        'post_types'	=> 'product',
        'context'    	=> 'normal',
        'priority'   	=> 'high',
        'fields' => array(
            array(
                'id'   => 'erp_custom_price',
                'name' => 'Inserire prezzo manualmente?',
                'type' => 'checkbox',
            ),
            array(
	            'id'     => 'erp_relation',
	            'type'   => 'group',
	            'clone'  => true,
	            // List of sub-fields
	            'fields' => array(
	            	array(
	                    'name' => 'Prodotto',
	                    'id'   => 'product',
	                    'type' => 'select',
	                    'options' => $selectErpProductList
	                ),
	                array(
	                    'name' => '% di riferimento',
	                    'id'   => 'scope',
	                    'type' => 'number',
	                    'std' => 100
	                ),
	                array(
	                    'name' => 'Qty per singolo pezzo',
	                    'id'   => 'qty',
	                    'type' => 'number',
	                    'std' => 1
	                ),
	                // Other sub-fields here
	            ),
	        ),
        ),
    );
    return $meta_boxes;
}





/**
 *
 * My Order Tracking
 *
 */
function infinity_wc_add_my_account_orders_column( $columns ) {

    $new_columns = array();

    foreach ( $columns as $key => $name ) {

        $new_columns[ $key ] = $name;

        // add ship-to after order status column
        if ( 'order-status' === $key ) {
            $new_columns['order-tracking'] = __( 'Tracking', 'infinity' );
            $new_columns['invoice'] = __( 'Invoice', 'infinity' );
        }
    }

    return $new_columns;
}
add_filter( 'woocommerce_my_account_my_orders_columns', 'infinity_wc_add_my_account_orders_column' );



function infinity_wc_my_orders_tracking_column( $order ) {
    $tracking_url = get_post_meta($order->get_id(), '_phn_tracking_code', true);
    if(!empty($tracking_url)) {
        echo $tracking_url;
    } else {
        echo '-';
    }
}
add_action( 'woocommerce_my_account_my_orders_column_order-tracking', 'infinity_wc_my_orders_tracking_column' );

function infinity_wc_my_orders_invoice_column( $order ) {
    $invoice_url = get_post_meta($order->get_id(), '_phn_order_invoice', true); 
    if(!empty($invoice_url)) {
        echo '<a href="'.$invoice_url.'" download class="woocommerce-button button view">Fattura</a>';
    } else {
        echo '-';
    }
}
add_action( 'woocommerce_my_account_my_orders_column_invoice', 'infinity_wc_my_orders_invoice_column' );