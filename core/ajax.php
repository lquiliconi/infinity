<?php



/*====================================
=            SYNC LISTINO            =
====================================*/

add_action( 'wp_ajax_phn_cron_sync_article', 'phn_ajax_cb_sync_product'  );
add_action( 'wp_ajax_nopriv_phn_cron_sync_article', 'phn_ajax_cb_sync_product');

function phn_ajax_cb_sync_product(){
    global $integration;
    global $wpdb;
    
    $ErpProductList = $integration->get_erp_product_list();
    var_dump($ErpProductList);
    die();
    
    $procedureResult = array('update' => 0, 'insert' => 0);
    // var_dump($ErpProductList);
    if( $ErpProductList ){
        $wpdb->update( 
            $wpdb->prefix.integrationClass::ERP_PRODUCT_TABLE_NAME, 
            array( 
                'active' => 0,
            ), 
            array( 'active' => 1 )
        );
    }


    foreach ($ErpProductList['articoli'] as $key => $item) {
        $currentItem = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix.integrationClass::ERP_PRODUCT_TABLE_NAME." WHERE code = '".$item['codice']."'");
        if( $currentItem ){
            $procedureResult['update']++;
            $wpdb->update( 
                $wpdb->prefix.integrationClass::ERP_PRODUCT_TABLE_NAME, 
                array( 
                    'vat' => $item['iva'] ,
                    'price' => $item['prezzo'],
                    'desc' => $item['descrizione'],
                    'active' => 1
                ), 
                array( 'code' => $item['codice'] )
            );
        }else{
            $procedureResult['insert']++;
            $wpdb->insert( 
                $wpdb->prefix.integrationClass::ERP_PRODUCT_TABLE_NAME, 
                array( 
                    'code' => $item['codice'], 
                    'vat' => $item['iva'] ,
                    'price' => $item['prezzo'],
                    'desc' => $item['descrizione'],
                    'active' => 1
                ), 
                array( 
                    '%s', 
                    '%d',
                    '%d',
                    '%s',
                    '%d'
                ) 
            );
        }
    }
    
    die(json_encode($procedureResult));


}

/*=====  End of SYNC LISTINO  ======*/




/*=====================================
=            PROCESS FSEC             =
=====================================*/

add_action( 'wp_ajax_phn_cron_process_fsec', 'phn_ajax_cb_process_fsec'  );
add_action( 'wp_ajax_nopriv_phn_cron_process_fsec', 'phn_ajax_cb_process_fsec');

function phn_ajax_cb_process_fsec(){
    global $integration;

    $result = $integration->process_fsec_ready_orders();
    echo '<pre>';
    var_dump($result);
    die();
    
}

/*=====  End of PROCESS FSEC   ======*/



/*=====================================
=            PROCESS FSEO             =
=====================================*/

add_action( 'wp_ajax_phn_cron_process_fseo', 'phn_ajax_cb_process_fseo'  );
add_action( 'wp_ajax_nopriv_phn_cron_process_fseo', 'phn_ajax_cb_process_fseo');

function phn_ajax_cb_process_fseo(){
    global $integration;

    $result = $integration->process_fseo_ready_orders();
    echo '<pre>';
    var_dump($result);
    die();
    
}

/*=====  End of PROCESS FSEO   ======*/


/*=====================================
=       PROCESS FILE TRACKING         =
=====================================*/

add_action( 'wp_ajax_phn_ajax_retrieve_tracking_file', 'phn_ajax_retrieve_tracking_file'  );
add_action( 'wp_ajax_nopriv_phn_ajax_retrieve_tracking_file', 'phn_ajax_retrieve_tracking_file');

function phn_ajax_retrieve_tracking_file(){
    $upld_url = wp_upload_dir();
    $path =  get_home_path().'/workingdata/chiapparoli/working/'; 
    $nextpath =  get_home_path().'/workingdata/chiapparoli/done/'; 
    $files=scandir($path); #Scansiono la directory
    $count_found_lines = 0;
    $count_processed_order = 0;
    foreach ($files as $key => $value) {
        if($value!="." && $value!=".."){
            $txt = file_get_contents($path."/".$value); #Recupero il contenuto dei txt
            $lines =  explode("\n", $txt);              #Suddivido le righe in un array di stringhe
            foreach ($lines as $line) {
                if(preg_match("/^(HSTPKH)/", $line)) {  #Controllo se la stringa è di intestazione (La H finale del pattern sta per intestazione, la D dettaglio)
                    $count_found_lines++;
                    $order_details = phn_read_tracking_string($line);
                    var_dump($order_details);
                    die();
                    if(!empty($order_details)) {
                        $order_to_edit = wc_get_order($order_details['codice_ordine']);
                        if($order_to_edit) {
                            if(!empty($order_details['tracking_link'])){
                                $tracking_code = $order_to_edit->update_meta_data( '_phn_tracking_code', $order_details['tracking_link'] );

                                $message = "L'ordine #".$order_details['codice_ordine']." è tracciabile tramite l'apposito link!<br>Puoi tracciare la tua spedizione <a href=\"".$order_details['tracking_link']."\" target=\"_blank\">qui</a>.";
                                $headers = array('Content-Type: text/html; charset=UTF-8');
                                $mailer = WC()->mailer();
                                $wrapped_message = $mailer->wrap_message(__('Il tuo ordine è ora traccabile','shop_phn'), $message);
                                $wc_email = new WC_Email;
                                $message = $wc_email->style_inline($wrapped_message);
                                wp_mail( $order_to_edit->get_billing_email(), __('Ordine #'.$order_details['codice_ordine'].' Tracciabile ', 'shop_phn'), $message, $headers );
                                $count_processed_order++; 
                            }                              
                        }                        
                    }
                   
                } 
            }
            if($count_found_lines > 0) {
                if($count_found_lines != $count_processed_order) {
                    error_log("###PHN-ERROR-tracking-".$value."### - INCONGRUENZA NEL PROCESSARE IL FILE");
                    $message = "C'è stato un errore nell'elaborare il file di spedizione \"".$value."\".<br>È consigliabile fare un controllo sia nell'error log che nel file stesso.";
                    $headers = array('Content-Type: text/html; charset=UTF-8');
                    $mailer = WC()->mailer();
                    $wrapped_message = $mailer->wrap_message(__('Errore di processing dei file del tracking code','shop_phn'), $message);
                    $wc_email = new WC_Email;
                    $message = $wc_email->style_inline($wrapped_message);
                    wp_mail( 'webmaster@holocron.it', '[ADMIN Shop PHN] - Errore nel processare trackings', $message, $headers );
                }  
                rename($path."/".$value, $nextpath."/".$value);                
            }

        }
    }
    die(); 
}

function phn_read_tracking_string($string) {
    $result_array = array();
    $phn_delimiters = array(
        array(
            'label' => 'codice_ordine',
            'start' => 49,
            'finish' => 58,
        ),
        /*array(
            'label' => 'codice_ordine',
            'start' => 12,
            'finish' => 31,
        ),*/
        array(
            'label' => 'codice_vettore',
            'start' => 82,
            'finish' => 84,
        ),
        array(
            'label' => 'tracking_link',
            'start' => 85,
            'finish' => 159,
        ),
        array(
            'label' => 'rif_spedizione',
            'start' => 160,
            'finish' => 180,
        ),
    );
    foreach($phn_delimiters as $del) {
        $del_len = $del['finish'] - $del['start'] +1;
        $result_array[$del['label']] = substr($string, $del['start']-1, $del_len);
    }
    return $result_array;
}


/*=====  End of PROCESS FILE TRACKING   ======*/

/*=====================================
=       PROCESS FILE INVOICE         =
=====================================*/

add_action( 'wp_ajax_phn_ajax_retrieve_invoice_file', 'phn_ajax_retrieve_invoice_file'  );
add_action( 'wp_ajax_nopriv_phn_ajax_retrieve_invoice_file', 'phn_ajax_retrieve_invoice_file');

function phn_ajax_retrieve_invoice_file(){
    $upld_url = wp_upload_dir();

    $path =  get_home_path().'/workingdata/infinity/working/'; 
    $nextpath =  get_home_path().'/workingdata/infinity/done/'; 

    $files = scandir($path); #Scansiono la directory
    $count_found_invoice = 0;
    $count_processed_invoice = 0;
    foreach ($files as $key => $value) {
        if($value!="." && $value!=".."){            
            if(preg_match("/(\.pdf)$/", $value)) {#È un pdf?
                $count_found_invoice++;
                $filename_noext = str_replace( ".pdf", "", $value ); #prendo il filename
                $order_to_edit = wc_get_order($filename_noext);
                $order_data = $order_to_edit->get_data();
                if($order_to_edit) {
                    $tracking_code = update_post_meta($order_to_edit->get_id(), '_phn_order_invoice', '/workingdata/infinity/done/'.$value);
                    $attachments = array($path."/".$value);
                    $message = "La fattura per l'ordine #".$order_to_edit->get_id()." è disponibile. Puoi scaricarla dalla tua sezione personale dello Shop.";
                    $headers = array('Content-Type: text/html; charset=UTF-8');
                    $mailer = WC()->mailer();
                    $wrapped_message = $mailer->wrap_message(__('La fattura del tuo ordine è disponibile','shop_phn'), $message);
                    $wc_email = new WC_Email;
                    $message = $wc_email->style_inline($wrapped_message);
                    wp_mail( $order_data['billing']['email'], __('Ordine #'.$order_to_edit->get_id().' Fattura', 'shop_phn'), $message, $headers, $attachments );
                    $count_processed_invoice++; 
                                                 
                }  

            }
           
                        
            if($count_found_invoice > 0) {
                if($count_found_invoice != $count_processed_invoice) {
                    error_log("###PHN-ERROR-tracking-".$value."### - INCONGRUENZA NEL PROCESSARE IL FILE");
                    $message = "Errore nel porocessare la fattura per l'ordine #\"".$value."\".<br> Non è stato possibile recuperare l'ordine relativo per inviare la mail al cliente.";
                    $headers = array('Content-Type: text/html; charset=UTF-8');
                    $mailer = WC()->mailer();
                    $wrapped_message = $mailer->wrap_message(__('Errore di processing di una fattura','shop_phn'), $message);
                    $wc_email = new WC_Email;
                    $message = $wc_email->style_inline($wrapped_message);
                    wp_mail( 'webmaster@holocron.it', '[ADMIN Shop PHN] - Errore nel processare una fattura', $message, $headers );
                }  
                rename($path."/".$value, $nextpath."/".$value);                
            }

        }
    }
    die(); 
}
/*=====  End of PROCESS FILE INVOICE   ======*/