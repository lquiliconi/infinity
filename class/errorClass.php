<?php 

/**
 * 
 */
class integrationErrorClass
{	

	const TYPE_ERROR 			= 2;
	const TYPE_WARNING 			= 1;
	const TYPE_NOTIFY   		= 0;


	const PROC_GENERAL 			= 0;
	const PROC_CHIAPPAROLI 		= 1;
	const PROC_INFINITY_ORDER 	= 2;
	const PROC_INFINITY_CLIENT 	= 3;
	const PROC_INFINITY_CRON	= 4;

	const MSG_DEFAULT 			= "Errore procedura";

	const LOG_REL_PATH					= "wp-content/logs/";
	const LOG_REL_PATH_CHIAPPAROLI		= "wp-content/logs/chiapparoli/";
	const LOG_REL_PATH_INFINITY			= "wp-content/logs/infinity/";

	const LOG_LEVEL_INFO 		= 0;
	const LOG_LEVEL_WARNING 	= 1;
	const LOG_LEVEL_ERROR 		= 2;

	const EMAIL_THRESHOLD 		= 1;



	private $emailBody 	= FALSE;
	private $type 		= FALSE;
	private $orderID 	= FALSE;
	private $process 	= FALSE;
	private $message 	= FALSE;
	private $data 	 	= FALSE;	

	private $loggerGeneral = FALSE;
	private $loggerChiapparoli = FALSE;
	private $loggerInfinity = FALSE;

	private $emailContactList 	= FALSE;

	
	function __construct(  $emailContact = FALSE )
	{
			
		$this->emailContactList 	= $emailContact;

		$this->loggerGeneral 		= new Katzgrau\KLogger\Logger( ABSPATH.self::LOG_REL_PATH );
		
		


	}



	public function log(  $type = self::TYPE_ERROR, $process = FALSE, $message = self::MSG_DEFAULT, $orderID = FALSE, $data = FALSE, $noMail = FALSE )
	{
			
		$this->type 	= $type;
		$this->process 	= $process;
		$this->message 	= $message;
		$this->data 	= $data;
		$this->orderID  = intval($orderID);
		
		$this->logData( $message , $data ,  $type , $process);
		
		if( $type >= self::EMAIL_THRESHOLD  ){
			if( $this->emailContactList  ){
				//$this->sendEmail();
			}else{
				$this->logData(  'Nessun indirizzo email per notificare il problema', FALSE, self::LOG_LEVEL_ERROR , self::PROC_GENERAL );
			}
		}


	}

	private function sendEmail( $to , $subject ){
		$this->emailBody = $this->buildMessageBody();

		$this->send_email_woocommerce_style(  $this->emailContactList , $this->build_email_subject(), $this->build_email_heading(), $this->emailBody );


	}






	private function buildMessageBody(){

		$body = '';

		
		switch ( $this->type ) {
			case self::LOG_LEVEL_INFO:
				$body .= '<p><b>Tipologia errore:</b> INFO</p>';
				break;

			case self::LOG_LEVEL_WARNING:
				$body .= '<p><b>Tipologia errore:</b> WARNING</p>';
				break;
			
			case self::LOG_LEVEL_ERROR:
				$body .= '<p><b>Tipologia errore:</b> ERROR</p>';
				break;
			default:
				$body .= '<p><b>Tipologia errore:</b> GENERICO</p>';
				break;
		}

		$body .= '<br>';


		switch ($this->process) {
			case self::PROC_GENERAL:
				$body .= '<p><b>Procedura:</b> GENERICA</p>';
				break;
			case self::PROC_CHIAPPAROLI:
				$body .= '<p><b>Procedura:</b>SYNC CHIAPPAROLI</p>';
				break;
			case self::PROC_INFINITY_CLIENT:
				$body .= '<p><b>Procedura:</b>SYNC INFINITY CLIENTE</p>';
				break;
			case self::PROC_INFINITY_ORDER:
				$body .= '<p><b>Procedura:</b>SYNC INFINITY ORDINE</p>';
				break;
			case self::PROC_INFINITY_CRON:
				$body .= '<p><b>Procedura:</b>CRON INFINITY</p>';
				break;
			default:
				$body.= '<p><b>Procedura:</b></p>';
				break;
		}

		$body .= '<br><hr>';

		$body.= '<p><b>Messaggio:</b></p>';
		$body.= '<p>'.$this->message.'</p>';

		$body .= '<br><hr>';

		$body.= '<p><b>Info Aggiuntive:</b></p>';
		$body.= '<p>'.json_encode($this->data).'</p>';

		$body .= '<br><hr>';

		if($this->orderID){
			$body.= '<p><b>Riferimento Ordine:</b> #'.$this->orderID.'</p>';
		}
		
		return $body;

	}


	private function build_email_subject(){

		return '[SHOP] - Notifica errore';

	}


	private function build_email_heading(){

		switch ($this->process) {
			case self::PROC_GENERAL:
				return 'Errore generico';
				break;
			case self::PROC_CHIAPPAROLI:
				return 'Errore Chiapparoli';
				break;
			case self::PROC_INFINITY_CLIENT:
				return 'Errore Infinity - Inseriemnto Cliente';
				break;
			case self::PROC_INFINITY_ORDER:
				return 'Errore Infinity - Inseriemnto Ordine';
				break;
			case self::PROC_INFINITY_CRON:
				return 'Errore Infinity - Schedule';
				break;
			default:
				return 'Errore';
				break;
		}

	}






	private function logData( $message , $data, $level = self::LOG_LEVEL_WARNING, $process = FALSE ){

		if( empty($data) ) $data = array();

		if( $process !== FALSE ){
			switch ($process) {
				case self::PROC_GENERAL:
					$message = '[GENERAL] '.$message;
					break;
				case self::PROC_CHIAPPAROLI:
					$message = '[CHIAPPAROLI] '.$message;
					break;
				case self::PROC_INFINITY_CLIENT:
					$message = '[INFINITY-CLIENT] '.$message;
					break;
				case self::PROC_INFINITY_ORDER:
					$message = '[INFINITY-ORDER] '.$message;
					break;
				case self::PROC_INFINITY_CRON:
					$message = '[INFINITY-CRON] '.$message;
					break;
				default:
					
					break;
			}
		}


		switch ( $level) {
			case self::LOG_LEVEL_INFO:
				$this->loggerGeneral->info( $message , $data );
				break;

			case self::LOG_LEVEL_WARNING:
				$this->loggerGeneral->warning( $message , $data );
				break;
			
			case self::LOG_LEVEL_ERROR:
				$this->loggerGeneral->error( $message , $data );
				break;
			default:
				$this->loggerGeneral->info( $message , $data );
				break;
		}

		return;


	}




	private function send_email_woocommerce_style( $email, $subject, $heading, $message ) {
	  $mailer = WC()->mailer();
	  $wrapped_message = $mailer->wrap_message($heading, $message);
	  $wc_email = new WC_Email;
	  $html_message = $wc_email->style_inline($wrapped_message);
	  wp_mail( $email, $subject, $html_message,'Content-Type: text/html; charset=UTF-8' );
	}




}



