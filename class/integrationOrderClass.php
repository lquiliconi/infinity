<?php 




/**
 * 
 */
class integrationOrderClass
{	

	const FLAG_FSC_KEY				= '_phn_flag_fsc';
	const FLAG_FSEC_KEY				= '_phn_flag_fsec';
	const FLAG_FSEO_KEY				= '_phn_flag_fseo';
	const ERROR_FSC_KEY				= '_phn_error_fsc';
	const ERROR_FSEC_KEY			= '_phn_error_fsec';
	const ERROR_FSEO_KEY			= '_phn_error_fseo';


	const FLAG_STATUS_DISABLE		= 0; // Indica che la procedura è disabilitata
	const FLAG_STATUS_READY			= 1; // Indica che la procedura è pronta ad essere eseguita
	const FLAG_STATUS_COMPLITED		= 2; // Indica che la procedura è andata a buon fine
	const FLAG_STATUS_ERROR			= 3; // Indica che la procedura ha generato errore
	const FLAG_STATUS_NOT_NEED		= 4; // Indica che la procedura non è necessaria

	const FLAG_NAME_DISABLE			= "Disabled"; 	// Indica che la procedura è disabilitata
	const FLAG_NAME_READY			= "Ready"; 			// Indica che la procedura è pronta ad essere eseguita
	const FLAG_NAME_COMPLITED		= "Ok"; 			// Indica che la procedura è andata a buon fine
	const FLAG_NAME_ERROR			= "Error"; 			// Indica che la procedura ha generato errore
	const FLAG_NAME_NOT_NEED		= "Undefined"; 		// Indica che la procedura non è necessaria


	const USER_ERP_ID 				= "_erp_client_id";
	const USER_BILLING_ERP_ID 		= "_erp_billing_id"; #MANCA#
	const USER_SHIPPING_ERP_ID 		= "_erp_shipping_id"; #MANCA#
	const USER_CF_META				= "_billing_cf";
	const ORDER_ERP_ID				= "_erp_order_id";

	const ERP_NEW_CLIENT_ACTION 	= "clienti";


	
	public $order 		= FALSE;
	public $orderMeta 	= FALSE;
	public $flagFSC 	= self::FLAG_STATUS_DISABLE;
	public $flagFSEC 	= self::FLAG_STATUS_DISABLE;
	public $flagFSEO 	= self::FLAG_STATUS_DISABLE;
	public $errorFSC 	= FALSE;
	public $errorFSEC 	= FALSE;
	public $errorFSEO 	= FALSE;

	private $ic 		= FALSE;



	

	/**
	 * 
	 */
	function __construct( $wcOrder = FALSE, &$ic = FALSE )
	{	

		if( is_int($wcOrder) ) $wcOrder = new WC_Order( intval($wcOrder) );

		$this->order = $wcOrder;
		$this->load_data();

		if( $ic ){
			$this->ic = $ic;
		}

	}



	function load_data(){

		if( !$this->order ) return FALSE;

		$orderMeta = get_post_meta( $this->order->get_id() );

		if( !empty( $orderMeta[ self::FLAG_FSC_KEY ]  ) ){
			$this->flagFSC = $orderMeta[ self::FLAG_FSC_KEY ][0];
		}

		if( !empty( $orderMeta[ self::FLAG_FSEC_KEY ]  ) ){
			$this->flagFSEC = $orderMeta[ self::FLAG_FSEC_KEY ][0];
		}

		if( !empty( $orderMeta[ self::FLAG_FSEO_KEY ]  ) ){
			$this->flagFSEO = $orderMeta[ self::FLAG_FSEO_KEY ][0];
		}

		if( !empty( $orderMeta[ self::ERROR_FSC_KEY ]  ) ){
			$this->errorFSC = $orderMeta[ self::ERROR_FSC_KEY ][0];
		}

		if( !empty( $orderMeta[ self::ERROR_FSEC_KEY ]  ) ){
			$this->errorFSEC = $orderMeta[ self::ERROR_FSEC_KEY ][0];
		}

		if( !empty( $orderMeta[ self::ERROR_FSEO_KEY ]  ) ){
			$this->errorFSEO = $orderMeta[ self::ERROR_FSEO_KEY ][0];
		}

	}


	public function init_data(){

		if( !$this->order ) return FALSE;

		$this->set_FSC( self::FLAG_STATUS_READY );
		$this->set_FSEC( self::FLAG_STATUS_READY );
		$this->set_FSEO( self::FLAG_STATUS_DISABLE );

	}










	public function get_FSC($orderID = FALSE){
		if( $orderID ) return get_post_meta( intval($orderID) , self::FLAG_FSC_KEY );
		return $this->flagFSC;
	}

	public function get_FSEC($orderID = FALSE){
		if( $orderID ) return get_post_meta( intval($orderID) , self::FLAG_FSEC_KEY );
		return $this->flagFSEC;
	}

	public function get_FSEO($orderID = FALSE){
		if( $orderID ) return get_post_meta( intval($orderID) , self::FLAG_FSEO_KEY );
		return $this->flagFSEO;
	}


	public function get_FSC_error($orderID = FALSE){
		if( $orderID ) return get_post_meta( intval($orderID) , self::ERROR_FSC_KEY );
		return $this->errorFSC;
	}

	public function get_FSEC_error($orderID = FALSE){
		if( $orderID ) return get_post_meta( intval($orderID) , self::ERROR_FSEC_KEY );
		return $this->errorFSEC;
	}

	public function get_FSEO_error($orderID = FALSE){
		if( $orderID ) return get_post_meta( intval($orderID) ,self::ERROR_FSEO_KEY  );
		return $this->errorFSEO;
	}




	public function set_FSC( $value = self::FLAG_STATUS_DISABLE ){
		if( !$this->order ) return FALSE;
		update_post_meta(  $this->order->get_id(), self::FLAG_FSC_KEY, intval($value) );
		$this->flagFSC = intval($value);
		return TRUE;
	}

	public function set_FSEC( $value = self::FLAG_STATUS_DISABLE ){
		if( !$this->order ) return FALSE;
		update_post_meta(  $this->order->get_id(), self::FLAG_FSEC_KEY, intval($value) );
		$this->flagFSEC = intval($value);
		return TRUE;
	}


	public function set_FSEO( $value = self::FLAG_STATUS_DISABLE ){
		if( !$this->order ) return FALSE;
		update_post_meta(  $this->order->get_id(), self::FLAG_FSEO_KEY, intval($value) );
		$this->flagFSEO = intval($value);
		return TRUE;
	}



	public function set_FSC_error( $error = FALSE ){
		if( !$this->order ) return FALSE;
		update_post_meta(  $this->order->get_id(), self::ERROR_FSC_KEY, trim($value) );
		$this->error_FSC = intval($value);
		return TRUE;
	}

	public function set_FSEC_error( $error = FALSE ){
		if( !$this->order ) return FALSE;
		update_post_meta(  $this->order->get_id(), self::ERROR_FSEC_KEY, trim($value) );
		$this->error_SEC = intval($value);
		return TRUE;
	}


	public function set_FSEO_error( $error = FALSE ){
		if( !$this->order ) return FALSE;
		update_post_meta(  $this->order->get_id(), self::ERROR_FSEO_KEY, trim($value) );
		$this->error_SEO = intval($value);
		return TRUE;
	}

	
	public function get_flag_status_label( $flagStatus = 0 ){
		
		switch ($flagStatus) {
			case self::FLAG_STATUS_DISABLE :
				return self::FLAG_NAME_DISABLE;
				break;

			case self::FLAG_STATUS_READY :
				return self::FLAG_NAME_READY;
				break;

			case self::FLAG_STATUS_COMPLITED :
				return self::FLAG_NAME_COMPLITED;
				break;

			case self::FLAG_STATUS_ERROR :
				return self::FLAG_NAME_ERROR;
				break;

			case self::FLAG_STATUS_NOT_NEED :
				return self::FLAG_NAME_NOT_NEED;
				break;
			
			default:
				return self::FLAG_NAME_DISABLE;
				break;
		}
	}





	public function send_client_data_to_erp( ){
		// controllo che posso processare questo ordine
		if( intval($this->get_FSEC()) != self::FLAG_STATUS_READY ) {
			return FALSE;
		}


		$infinityClient = new infinityClass( $this->ic->integrationOption["infinity_appid"] , $this->ic->integrationOption["infinity_apikey"] , $this->ic->integrationOption["infinity_url"] );


		if( !$this->need_address_sync() ) return "User is synchronized";

		$requestBody = array( "clienti" => array($this->generateClientJson(  )) ) ;
		$response = $infinityClient->sendRequest( SELF::ERP_NEW_CLIENT_ACTION, $requestBody, 'POST' );

		//var_dump($response);
		//error_log(json_encode(($response)));

		
		if ($infinityClient->esito == "OK") {
			$this->set_FSEC( self::FLAG_STATUS_COMPLITED );
			update_post_meta($this->order->get_id(), self::USER_ERP_ID, $response['risultati'][0]['cliente']);

			//update_post_meta($this->order->get_id(), self::USER_BILLING_ERP_ID, $response['nomecampobillingerp']); #Salvo codice anagrafica ERP anche in ordine ##CAMBIA QUA##
			//update_post_meta($this->order->get_id(), self::USER_SHIPPING_ERP_ID, $response['nomecamposhippingerp']); #Salvo codice anagrafica ERP anche in ordine ##CAMBIA QUA##

			if( $this->order->get_user_id()  ){
				update_user_meta($this->order->get_user_id(), self::USER_ERP_ID, $response['risultati'][0]['cliente']);
				//update_user_meta( $this->order->get_user_id(), self::USER_BILLING_ERP_ID, $response['nomecampobillingerp']  ); #Salvo codice anagrafica ERP se utente esiste ##CAMBIA QUA##
				//update_user_meta( $this->order->get_user_id(), self::USER_SHIPPING_ERP_ID, $response['nomecamposhippingerp']  ); #Salvo codice anagrafica ERP se utente esiste ##CAMBIA QUA##
			}
		} else {
			$this->set_FSEC( self::FLAG_STATUS_ERROR );
			$this->set_FSEC_error( $infinityClient->errorMessage);
		}
		return $response;
		
		//$this->set_FSEC( self::FLAG_STATUS_COMPLITED )
		/*
		if( $this->order->get_user_id()  ){
			update_user_meta( $this->order->get_user_id() , self::USER_BILLING_ERP_ID, '-'  );
			update_user_meta( $this->order->get_user_id() , self::USER_SHIPPING_ERP_ID, '-'  );
		}

		return $response;

		*/

		// $r = $infinityClient->sendRequest(  'articoli' , FALSE , GET ,  1 );

		// return 'HELLO';
	}



	public function send_order_data_to_erp( ){
		// controllo che posso processare questo ordine

		// if( $this->get_FSEC() != self::FLAG_STATUS_COMPLITED ) return FALSE;
		// if( $this->get_FSEO() != self::FLAG_STATUS_READY ) return FALSE;


		$infinityClient = new infinityClass( $this->ic->integrationOption["infinity_appid"] , $this->ic->integrationOption["infinity_apikey"] , $this->ic->integrationOption["infinity_url"] );


		// if( !$this->need_address_sync() ) return "User is synchronized";

		$requestBody = array( "ordini" => array($this->generateOrderJson()) ) ;
		var_dump($requestBody);
	

		$response = $infinityClient->sendRequest( SELF::ERP_NEW_CLIENT_ACTION , $requestBody , "POST" );
		error_log(json_encode($response));
		var_dump($response);
		die();

		if ($infinityClient->esito == "OK") {
			$this->set_FSEO( self::FLAG_STATUS_COMPLITED );
			update_post_meta( $this->order->get_id(), self::ORDER_ERP_ID, $response['nomecampoORDERIDerp']  ); ##CAMBIA QUA##
			
		} else {
			$this->set_FSEO( self::FLAG_STATUS_ERROR );
			$this->set_FSEO_error( $infinityClient->errorMessage);
		}

		return $response;
		//echo json_encode($requestBody);


		//return $requestBody;

		//$this->set_FSEC( self::FLAG_STATUS_COMPLITED )

		// $r = $infinityClient->sendRequest(  'articoli' , FALSE , GET ,  1 );

		// return 'HELLO';
	}






	public function need_address_sync( ){

		if( !$this->order->get_user( ) ) return true;
		$userID = $this->order->get_user_id();
		$erp_billing = get_usermeta( $userID, self::USER_BILLING_ERP_ID );
		$erp_shipping = get_usermeta( $userID, self::USER_SHIPPING_ERP_ID );
		if(!empty($erp_billing) && !empty($erp_shipping)) {
			$this->set_FSEC( self::FLAG_STATUS_COMPLITED );
			update_post_meta($this->order->get_id(), self::USER_BILLING_ERP_ID, $erp_billing); #Salvo codice anagrafica ERP anche in ordine
			update_post_meta($this->order->get_id(), self::USER_SHIPPING_ERP_ID, $erp_shipping); #Salvo codice anagrafica ERP anche in ordine
		}

		return true;
	}





	private function generateClientJson(){

		$order = $this->order;


		$myuser_id = (int)$order->get_user_id();
		if($myuser_id) {
			$all_meta_for_user = array_map( function( $a ){ return $a[0]; }, get_user_meta( $myuser_id ) );
		} else {
			$all_meta_for_user = array_map( function( $a ){ return $a[0]; }, get_post_meta( $order->get_id() ) );
		}
		$branchAddresses = $this->generateAddress($order->get_id());

		


		$clientJSON = [];
		$clientJSON['progressivo'] = $order->get_id();

		$clientJSON['tipologia'] = 'P'; // A per azienda
		//$clientJSON['tipologia'] = $all_meta_for_user['_company_or_private_billing'];

		$clientJSON['nome'] = $this->order->get_billing_first_name();
		$clientJSON['cognome'] = $this->order->get_billing_last_name();


		$clientJSON['ragioneSociale'] = ( $clientJSON['tipologia'] == 'P' ) ? NULL : $all_meta_for_user['billing_company'] ;
		$clientJSON['partitaIva'] = ( $clientJSON['tipologia'] == 'P' ) ? NULL : $all_meta_for_user['_p_iva_billing'] ;

		$clientJSON['codiceFiscale'] = ( $clientJSON['tipologia'] == 'A' ) ? NULL : strtoupper( $this->order->get_meta( self::USER_CF_META )  ) ;


		$clientJSON['email'] = $this->order->get_billing_email();
		$clientJSON['phone'] = $this->order->get_billing_phone();

		$clientJSON['codiceInfinity'] = $this->get_user_erp_client_code();


		$clientJSON['sedi'] = array(
			array(
				"codice" => "1",
				"indirizzo" => $this->order->get_billing_address_1(),
				"localita" => $this->order->get_billing_city(),
				"provincia" => $this->order->get_billing_state(),
				"cap" => $this->order->get_billing_postcode(),
				"nazione" => $this->order->get_billing_country()
			),

			array(
				"codice" => "2",
				"indirizzo" => $this->order->get_shipping_address_1(),
				"localita" => $this->order->get_shipping_city(),
				"provincia" => $this->order->get_shipping_state(),
				"cap" => $this->order->get_shipping_postcode(),
				"nazione" => $this->order->get_shipping_country()
			)
		);

		return $clientJSON;   

	}

	

	public function generateOrderJson(){


		$order = $this->order;



		$branchAddresses = $this->generateAddress($order->get_id());
		$customerID = get_post_meta( $order_number, '_customer_user', true );


		if($order->get_user_id()){
			$codiceInfinity = get_user_meta( $order->get_user_id(), self::USER_ERP_ID, true);
		} else {
			$codiceInfinity = get_post_meta($order->get_id(), self::USER_ERP_ID, true);
		}
		
		error_log('CODICE INFINITY orderJSON: '.$codiceInfinity);

		//JSON ORDER
		$orderJSON = [];
		$orderJSON['progressivo'] = (int)$this->order->get_id();
		$orderJSON['codiceCliente'] = $codiceInfinity;
		$orderJSON['sedeFatturazione'] = "1";//$this->getBranchID($order->get_billing_address_1(), $branchAddresses);//"1";//get_user_meta( $customerID, self::USER_BILLING_ERP_ID, true);
		$orderJSON['sedeSpedizione'] = "2";//$this->getBranchID($order->get_billing_address_1(), $branchAddresses);//"2";//get_user_meta( $customerID, self::USER_SHIPPING_ERP_ID, true);
		$rowNumber = 1;

		$orderJSON['righe'] = array();


		$erpProductList = $this->get_erp_product_for_order();
		foreach ($erpProductList as $k => $product) {

			$tmp = array();
			$tmp['numeroRiga'] = $rowNumber;
			$tmp['articolo'] = $product->code;
			$tmp['quantità'] = $product->qty;
			$tmp['prezzo'] = $product->price;
			$tmp['iva'] = $product->vat;
			$tmp['totaleRiga'] =  $product->qty * $product->price;
			$orderJSON['righe'][] = $tmp;
			$rowNumber++;

			
		}


		$cart_value = number_format( (float) $this->order->get_total() - $this->order->get_total_tax() - $this->order->get_total_shipping() - $this->order->get_shipping_tax(), wc_get_price_decimals(), '.', '' );



		// $orderJSON['tipologiaSpedizione'] = $order->get_payment_method();
		// // if ( (int)$order->get_shipping_total() ) {
		// //    $orderJSON['costoSpedizione'] = (int)$order->get_shipping_total();
		// // }
		$orderJSON['costoSpedizione'] = 0;
		$orderJSON['ivaSpedizione'] = "10";//( $orderShippingCost ) ? round( ( $order->get_shipping_tax() / $order->get_shipping_total() )*100 ) : NULL;
		$orderJSON['totaleImponibileOrdine'] = $cart_value;
		$orderJSON['importoTotaleOrdine'] = $order->get_total();
		//error_log($orderJSON['importoTotaleOrdine']);

		return $orderJSON;

	}



	public function get_erp_product_for_order(){

		$erpProducts = array();
		
		foreach ($this->order->get_items() as $index => $singleProduct) {
			echo '<pre>';
		 	
		 	$singleProductErp = get_post_meta( $singleProduct->get_product_id() , 'erp_relation', true );
		 	
		 	// var_dump($singleProductErp);

		 	foreach ($singleProductErp as $key => $erpProduct) {

		 		$erpProductObject = $this->get_erp_product( $erpProduct["product"] );
		 		// var_dump( $erpProduct);
		 		// var_dump($erpProductObject);
		 		$erpProductObject->qty = $erpProduct['qty'] * $singleProduct->get_quantity();
		 		$erpProductObject->price = $erpProductObject->price * ( $erpProduct['scope'] / 100 );
		 		// var_dump($erpProductObject);
		 		$erpProducts[] = $erpProductObject;
		 		# code...
		 	}


		}

		return $erpProducts;

	}




	private function get_order_meta( $key ){

		$tmp = $this->order->get_meta( $key );
		if( $tmp ) return $tmp[0];
		else return false;
	}


	public function get_user_erp_client_code(){

		if( !$this->order->get_user_id() ) return NULL;
		$userid = get_user_meta( $this->order->get_user_id() , self::USER_ERP_ID  , true );
		if(empty($userid)) {
			$userid = NULL;
		}
		return $userid;

	}






	public function get_erp_product( $code = FALSE ){
		global $wpdb;

		if( empty($code) ) return FALSE;

		$currentItem = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix.integrationClass::ERP_PRODUCT_TABLE_NAME." WHERE code = '".$code."'");

		return $currentItem ;
	}





}