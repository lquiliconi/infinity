<?php 


/**
 * 
 */
class integrationClass 
{

	const GENERAL_OPTION_KEY 		= 'phnshop_main_settings';
	const INTEGRATION_OPTION_KEY 	= 'phnshop_integration_settings';

	const ERP_PRODUCT_TABLE_NAME 	= 'erp_products';

	// Contiene tutte le impostazioni generali. Wp Global Option
	public $generalOption 		= FALSE;

	// Contiene tutte le impostazioni per le integrazioni. Wp Global Option
	public $integrationOption 	= FALSE;


	public $errorClass 			= FALSE;
	



	function __construct()
	{	
		$this->load_options();
		$this->init_filter();
		$this->init_action();
		$this->errorClass = new integrationErrorClass( $this->generalOption["support_email"] );
	}


	public function load_options(){
		$this->generalOption 		= get_option( self::GENERAL_OPTION_KEY );
		$this->integrationOption 	= get_option( self::INTEGRATION_OPTION_KEY );
		// var_dump($this->integrationOption);
		// die();
	}


	private function init_filter(){
		add_filter( 'manage_edit-shop_order_columns', array( $this , 'add_order_columns') );
		add_filter( 'manage_shop_order_posts_custom_column', array( $this , 'content_order_columns') , 20 );
	}

	private function init_action(){
		add_action( 'woocommerce_process_shop_order_meta',  array( $this , 'init_order_data') ); 
		add_action( 'woocommerce_new_order' ,  array( $this , 'init_order_data') ); 
		add_action( 'woocommerce_admin_order_data_after_order_details', array( $this , 'order_details_data')  );
		add_action( 'admin_menu', array( $this , 'add_erp_product_page') );
		add_action( 'save_post', array( $this , 'alter_product_price_based_on_items'),999);
	}

	






	public function add_order_columns( $columns ){
		unset($columns['order_total']);
		// $columns['col_fsc'] 		= 'Chiapparoli';
		$columns['col_fsec'] 		= 'ERP Cliente';
		$columns['col_fseo'] 		= 'ERP Ordine';
		$columns['col_tracking'] 	= 'Tracking';
		$columns['order_total'] 	= 'Totale';
    	return $columns;
	}


	public function content_order_columns( $column ){
		global $post;

		switch ($column) {
			case 'col_fsc':
				switch ( get_post_meta( $post->ID, integrationOrderClass::FLAG_FSC_KEY, TRUE ) ) {
					case integrationOrderClass::FLAG_STATUS_DISABLE :
						?>
						<mark class="order-status status-pending"><span>Disabled</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_READY :
						?>
						<mark class="order-status status-on-hold"><span>Ready</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_COMPLITED :
						?>
						<mark class="order-status status-processing"><span>Ok</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_ERROR :
						?>
						<mark class="order-status status-failed"><span>Error</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_NOT_NEED :
						?>
						<mark class="order-status status-pending"><span>Ok</span></mark>
						<?php
						break;
					
					default:
						?>
						<mark class="order-status status-pending"><span>Undefined</span></mark>
						<?php
						break;
				}
				break;
			case 'col_fsec':
				switch ( get_post_meta( $post->ID, integrationOrderClass::FLAG_FSEC_KEY, TRUE ) ) {
					case integrationOrderClass::FLAG_STATUS_DISABLE :
						?>
						<mark class="order-status status-pending"><span>Disabled</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_READY :
						?>
						<mark class="order-status status-on-hold"><span>Ready</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_COMPLITED :
						?>
						<mark class="order-status status-processing"><span>Ok</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_ERROR :
						?>
						<mark class="order-status status-failed"><span>Error</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_NOT_NEED :
						?>
						<mark class="order-status status-pending"><span>Ok</span></mark>
						<?php
						break;
					
					default:
						?>
						<mark class="order-status status-pending"><span>Undefined</span></mark>
						<?php
						break;
				}
				break;
			case 'col_fseo':
				switch ( get_post_meta( $post->ID, integrationOrderClass::FLAG_FSEO_KEY, TRUE ) ) {
					case integrationOrderClass::FLAG_STATUS_DISABLE :
						?>
						<mark class="order-status status-pending"><span>Disabled</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_READY :
						?>
						<mark class="order-status status-on-hold"><span>Ready</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_COMPLITED :
						?>
						<mark class="order-status status-processing"><span>Ok</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_ERROR :
						?>
						<mark class="order-status status-failed"><span>Error</span></mark>
						<?php
						break;

					case integrationOrderClass::FLAG_STATUS_NOT_NEED :
						?>
						<mark class="order-status status-pending"><span>Ok</span></mark>
						<?php
						break;
					
					default:
						?>
						<mark class="order-status status-pending"><span>Undefined</span></mark>
						<?php
						break;
				}
				break;
			case "col_tracking": 
				echo '--';
				break;
		}

	}



	public function init_order_data( $order_id ){

		if( !$order_id ) return;
		
		$flagFSC = get_post_meta( $order_id ,integrationOrderClass::FLAG_FSC_KEY, TRUE );
		if( $flagFSC === "" ){
			$IOC = new integrationOrderClass( $order_id );
			$IOC->init_data();
			error_log('Initing data for order '.$order_id);
		}
		
	}





	public function order_details_data( $order ){

		?>
 
		<br class="clear" />
		<h4>Sincronizzazione<a href="#" class="edit_address">Modifica</a></h4>
		<?php 
			$IOC = new integrationOrderClass( $order );
		?>
		<div class="address">
			
			<!-- <p><strong>Chiapparoli:</strong> <?php echo $IOC->get_flag_status_label( $IOC->get_FSC() ) ?></p> -->
			<p><strong>ERP Cliente:</strong> <?php echo $IOC->get_flag_status_label( $IOC->get_FSEC() ) ?></p>
			<p><strong>ERP Ordine:</strong> <?php echo $IOC->get_flag_status_label( $IOC->get_FSEO() ) ?></p>
			
		</div>
		<div class="edit_address"><?php
 
			// woocommerce_wp_radio( array(
			// 	'id' => 'is_gift',
			// 	'label' => 'Is this a gift order?',
			// 	'value' => $is_gift,
			// 	'options' => array(
			// 		'' => 'No',
			// 		'1' => 'Yes'
			// 	),
			// 	'style' => 'width:16px', // required for checkboxes and radio buttons
			// 	'wrapper_class' => 'form-field-wide' // always add this class
			// ) );
 
			// woocommerce_wp_select( array(
			// 	'id' => 'gift_wrap',
			// 	'label' => 'Gift Wrap:',
			// 	'value' => $gift_wrap,
			// 	'options' => array(
			// 		'' => 'No Wrap',
			// 		'Basic Wrap' => 'Basic Wrap',
			// 		'Magic Wrap' => 'Magic Wrap'
			// 	),
			// 	'wrapper_class' => 'form-field-wide'
			// ) );
 
			// woocommerce_wp_text_input( array(
			// 	'id' => 'gift_name',
			// 	'label' => 'Recipient name:',
			// 	'value' => $gift_name,
			// 	'wrapper_class' => 'form-field-wide'
			// ) );
 
			// woocommerce_wp_textarea_input( array(
			// 	'id' => 'gift_message',
			// 	'label' => 'Gift message:',
			// 	'value' => $gift_message,
			// 	'wrapper_class' => 'form-field-wide'
			// ) );
 
		?></div>
 
 
	<?php


	}


	/*=====================================
	=            PROCESS FSEC             =
	=====================================*/
	
	public function process_fsec_ready_orders(){

		$fsecOrders = wc_get_orders(  array( 'status' => 'processing' ) );
		$r = array();
		foreach ($fsecOrders as $k => $order) {
			$oic = new integrationOrderClass( $order, $this );
			$r[ $order->get_ID() ] = $oic->send_client_data_to_erp( );
		}

		return $r;

	}
	
	
	/*=====  End of PROCESS FSEC   ======*/




	/*=====================================
	=            PROCESS FSEO             =
	=====================================*/
	
	public function process_fseo_ready_orders(){

		$fseoOrders = wc_get_orders(  array( 'status' => 'processing' ) );
		$r = array();
		foreach ($fseoOrders as $k => $order) {
			$oic = new integrationOrderClass( $order, $this );
			$r[ $order->get_ID() ] = $oic->send_order_data_to_erp( );
		}

		return $r;

	}
	
	
	/*=====  End of PROCESS FSEO   ======*/
	




	/*====================================
	=            SYNC LISTINI            =
	====================================*/
	
	public function get_erp_product_list(){
		

		$infinityClient = new infinityClass( $this->integrationOption["infinity_appid"] , $this->integrationOption["infinity_apikey"] , $this->integrationOption["infinity_url"] );



		$r = $infinityClient->sendRequest(  'articoli' , FALSE , 'GET' ,  1 );

		// $responseData = json_decode($r,TRUE);
		$responseData = $r;

		if( is_array($responseData) ){
			if( $responseData['risultato']['ESITO'] == 'OK' ){
				return  $responseData;
			}else{
				$this->errorClass->log(1,4,'Errore raccolta codici prodotto');
				return FALSE; // Gestione errore?
			}
		}else{
			$this->errorClass->log(1,4,'Errore raccolta codici prodotto');
			return FALSE;
		}
		
	}
	
	
	/*=====  End of SYNC LISTINI  ======*/
	



	public function get_erp_products(){
		global $wpdb;

		$erpProductList  = $wpdb->get_results( 
			"
			SELECT * 
			FROM ".$wpdb->prefix.self::ERP_PRODUCT_TABLE_NAME."
			"
		);

		return $erpProductList;
	}


	public function get_erp_product( $code = FALSE ){
		global $wpdb;

		if( empty($code) ) return FALSE;

		$currentItem = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix.self::ERP_PRODUCT_TABLE_NAME." WHERE code = '".$code."'");

		return $currentItem ;
	}







	public function add_erp_product_page(){
		add_submenu_page( 'edit.php?post_type=product','Listino Interno', 'Listino Interno', 'manage_options', 'erp_products', array( $this , 'render_erp_product_page'));
	}




	public function render_erp_product_page(){
		?>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
		<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
		<script>
			$(document).ready(function() {
     			$('#erp_product_list').DataTable();
 			} );
		</script>
		<div class="wrap">
			<h2>Listino Interno prodotti</h2>
			<p>La sincronizzazione avviene in automatico ogni 24H (ore 12:00)</p>
			<hr>
			<table id="erp_product_list" class="widefat fixed" style="width:100%">
		        <thead>
		            <tr>
		                <th>COD</th>
		                <th>Descrizione</th>
		                <th>Prezzo</th>
		                <th>Iva</th>
		                <th>Data Attivazione</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<?php foreach ($this->get_erp_products() as $k => $erpProduct) { ?>
			        	<tr>
			                <td><b><?php echo $erpProduct->code ?></b></td>
			                <td><?php echo $erpProduct->desc ?></td>
			                <td><?php echo $erpProduct->price ?>€</td>
			                <td><?php echo $erpProduct->vat ?>%</td>
			                <td><?php echo $erpProduct->insert_at ?></td>
			            </tr>
		        	<?php } ?>
		            
		        </tbody>
		        <tfoot>
		            <tr>
		                <th>COD</th>
		                <th>Descrizione</th>
		                <th>Prezzo</th>
		                <th>Iva</th>
		                <th>Data Attivazione</th>
		            </tr>
		        </tfoot>
		    </table>
		</div>
		<?php
	}



	public function alter_product_price_based_on_items(){
		global $post;
		$totalPrice = 0;

		$enableCustomPrice = get_post_meta( $post->ID , 'erp_custom_price',TRUE );

		if( $enableCustomPrice ) return;

		$internalItems = get_post_meta( $post->ID , 'erp_relation',TRUE );



		foreach ($internalItems as $key => $item) {
			$erpItem = $this->get_erp_product( $item["product"] );
			if( $erpItem ){
				$ratio = $item["scope"] / 100;
				$partPrice = $erpItem->price * $ratio;
				$totalPrice += $partPrice*$item["qty"];
			}	
		}
		if( $totalPrice > 0 ){
			update_post_meta( $post->ID , '_regular_price', number_format((float)$totalPrice, 2, '.', '') );
			update_post_meta( $post->ID , '_price', number_format((float)$totalPrice, 2, '.', '') );
		}
	}





}	