<?php 
/**
 * 
 */
class infinityClass
{	
	const DEFAULT_PORT = 80;

	private $appID = FALSE;
	private $apiKey = FALSE;
	private $url    = FALSE;
	private $ready = FALSE;


   public $esito  = FALSE;
   public $errorMessage = FALSE;
   public $responsePayload = FALSE;


   const KO_RESULT = 'KO';
   const OK_RESULT = 'OK';
	
	function __construct( $appID , $apiKey, $url )
	{
		
		if( empty( $appID ) ) return $this->build_error_response( 010 , "Nesssuna App Id inserita" );
		if( empty( $apiKey ) ) return $this->build_error_response( 011 , "Nesssuna apiKey inserita" );
		if( empty( $url ) ) return $this->build_error_response( 012 , "Nesssun url inserito" );

		$this->appID 	= $appID;
		$this->apiKey 	= $apiKey;
		$this->url 		= $url;
		$this->port 	= $this->extract_port( $this->url );
		$this->ready 	= TRUE;
	}





	
	public function sendRequest(  $action , $body = FALSE , $method  = "GET" ,  $vers = 1){
      
		if( !$this->ready  ) return $this->build_error_response( 020 , 'Inizializzazione non avvenuta' );
		if( empty( $action )  ) return $this->build_error_response( 021 , 'Nessuna azione stabilita' );
		if( empty( $method )  ) return $this->build_error_response( 022 , 'Nessuna metodo indicato' );
   	
   	if( $body ){
   		$jsonBody = json_encode($body);
   	}else{
   		$jsonBody = FALSE;
   	}
   	
   	$reqUrl = $this->build_request_url( $action , $vers );
      
   	$tokenAMX = $this->generateAMX( $jsonBody , $url );

     	$curl = curl_init();
      
		curl_setopt_array($curl, array(
			CURLOPT_PORT => $this->port,
			CURLOPT_URL => $reqUrl,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 60,
         CURLINFO_HEADER_OUT => TRUE,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => strtoupper( $method ),
			CURLOPT_POSTFIELDS => $jsonBody,
			CURLOPT_HTTPHEADER => array(
				"Authorization: amx ".$tokenAMX,
				"Cache-Control: no-cache",
				"Content-Type: application/json",
				"Content-Length: " . strlen($jsonBody)
			),
		));

		$response = curl_exec($curl);

     
		curl_close($curl);

      /*var_dump($response);
      var_dump($reqUrl);
      var_dump($jsonBody);


      die("DOH");*/

      return $this->evaluateResponse( $response );
	}




	private function build_request_url( $action , $ver){
		return $this->url.'?'.http_build_query( array( 'action' => $action, 'ver' => $ver ) );
	}





   private function evaluateResponse( $response ){

      if( empty( $response ) || !json_decode($response,TRUE) ) {
         $this->esito = self::KO_RESULT;
         $this->errorMessage = "Risposta infinity non formattata correttamente";
         return FALSE;
      }

      $response = json_decode($response,TRUE);
       // echo '<pre>';
       // var_dump( $response);
      if( $response['risultato']["ESITO"] == self::KO_RESULT ){
         $this->esito = self::KO_RESULT;
         $this->errorMessage = $response['risultato']["MOTIVO"];
         return FALSE;
      }else{
         $this->esito = self::OK_RESULT;
         $this->responsePayload = $response['risultati'];
         return $response;
      }






   }



	private function generateAMX( $requestBody , $url, $method = "GET" ){

      $key = $this->appID;
      $base64Secret = $this->apiKey;

      $time = strtotime('NOW');
      $time = strval($time);
      //$time = '1525250500';
      $nonce = $this->newGuid();
      //$nonce = 'b3b0c80e0838909389b41b393c6e0138';
      // $method = 'POST';//$request->method;
      $encodedUri = strtolower(urlencode($url));
      $firstpass = true;

      $b64BodyContent = "";
      if ( $requestBody ) {
         $b64BodyContent = base64_encode(md5(json_encode($requestBody), true));
      }

      $rawSignature[] = $key;
      $rawSignature[] = $method;
      $rawSignature[] = $encodedUri;
      $rawSignature[] = $time;
      $rawSignature[] = $nonce;
      $rawSignature[] = $b64BodyContent;
      $rawSignatureStr = implode("",$rawSignature);

      $hash = hash_hmac("sha256", $rawSignatureStr, base64_decode($base64Secret), true );
      $signature = base64_encode($hash);

      return $key.':'.$signature.':'.$nonce.':'.$time;
   
	}


	private function newGuid() {
      $subject = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx';
      $arraySubject =  str_split($subject);
      $returnArray = [];
      foreach ($arraySubject as $index => $singleChar) {
         if ( $singleChar == 'x' ) {
            $returnArray[$index] = $this->randomX();
         } else {
            $returnArray[$index] = $this->randomY();
         }
      }
      $returnString = implode("",$returnArray);
      return $returnString;
   }



   private function build_error_response( $code, $message ){
   		return array( 'error' => array( 'code' => $code , 'desc' => $message ) );
   } 


   private function extract_port( $url ){
        
   		if( empty($url) ) return self::DEFAULT_PORT;
   		$urlComponents = parse_url( $url );
   		if(!$url) return self::DEFAULT_PORT;
   		if(empty($urlComponents['port'])) return self::DEFAULT_PORT;
   		return $urlComponents['port'];
   }


   private function randomX() {
      //error_log('RandomX: '.base_convert( ( mt_rand(0,9)/10 * 16 ) | 0, 10, 16 ));
      return (int)base_convert( ( mt_rand(0,9)/10 * 16 ) | 0, 10, 16 );
   }

   private function randomY() {
      //error_log('RandomY: '.base_convert( randomX() & 3 | 8, 10, 16 ));
      return (int)base_convert(  $this->randomX() & 3 | 8, 10, 16 );
   }


}